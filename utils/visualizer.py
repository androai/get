import torchvision
from torch.utils.tensorboard import SummaryWriter


class Visualizer:
    def __init__(self, opt):
        self.opt = opt
        self.port = opt.display_port
        self.writer = SummaryWriter()
    def display_current_results(self, visuals, steps):
        for label, image in visuals.items():
            self.writer.add_image(label, torchvision.utils.make_grid(image), steps)
    def plot_current_losses(self, total_iters, losses):
        for label, loss in losses.items():
            self.writer.add_scalar(label, loss, total_iters)
    def print_current_losses(self, epoch, iters, losses, t_comp, t_data):
        message = '(epoch: %d, iters: %d, data: %.3f, comp: %.3f) ' % (epoch, iters, t_data, t_comp)
        for k, v in losses.items():
            message += '%s: %.5f ' % (k, v)
        print(message)
