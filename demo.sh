#!/bin/sh
set -eu

video="data/video"
audio="data/test_audio"

rm -rf $audio andro.mp4
mkdir -p $audio/audio
mkdir -p $audio/feature
cp andro.wav $audio/audio/andro.wav

python3 vendor/ATVGnet/code/test.py -i $audio/ 2> /dev/null

python3 demo_exp.py \
    --dataset_mode audio_expression \
    --data_dir $audio \
    --net_dir $video 2> /dev/null

python3 demo_act.py --src_dir $audio --tgt_dir $video 2> /dev/null

python3 vendor/neural-face-renderer/test.py \
    --model test \
    --netG unet_256 \
    --direction BtoA \
    --dataset_mode temporal_single \
    --norm batch \
    --input_nc 21 \
    --Nw 7 \
    --preprocess none \
    --eval \
    --use_refine \
    --name nfr \
    --checkpoints_dir $video/checkpoints \
    --dataroot $audio/reenact \
    --results_dir $audio \
    --load_size 256 2> /dev/null

python3 demo_com.py --src_dir $audio --tgt_dir $video 2> /dev/null

/usr/bin/ffmpeg -y -hide_banner -loglevel panic \
    -thread_queue_size 8192 -i $audio/audio/andro.wav \
    -thread_queue_size 8192 -i $audio/comp/%05d.png \
    -vcodec libx264 -preset slower -profile:v high -crf 18 -pix_fmt yuv420p \
    andro0.mp4 && ext=$(ffprobe -i andro0.mp4 -show_entries format=duration -v quiet -of csv="p=0")

/usr/bin/ffmpeg -y -hide_banner -loglevel panic -ss 0.1 -i andro0.mp4 \
    -to $(awk "BEGIN{print ($ext - 0.5)}") andro.mp4 && rm andro0.mp4 && mv andro.mp4 ../