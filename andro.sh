#!/bin/sh -ex
cat << 'EOM'

                        $$\                             $$\ 
                        $$ |                            \__|
 $$$$$$\ $$$$$$$\  $$$$$$$ |$$$$$$\  $$$$$$\    $$$$$$\ $$\ 
 \____$$\$$  __$$\$$  __$$ $$  __$$\$$  __$$\   \____$$\$$ |
 $$$$$$$ $$ |  $$ $$ /  $$ $$ |  \__$$ /  $$ |  $$$$$$$ $$ |
$$  __$$ $$ |  $$ $$ |  $$ $$ |     $$ |  $$ | $$  __$$ $$ |
\$$$$$$$ $$ |  $$ \$$$$$$$ $$ |     \$$$$$$  $$\$$$$$$$ $$ |
 \_______\__|  \__|\_______\__|      \______/\__\_______\__|
                                                            
EOM
token='ya29.c.Kp8BCgiznbRZcxtjkxFbYDyCXXfjyPxj1q5qJBX7MiKLngesE8x675gtQh30Hi_iJqA4M3cL4vujx5u3tO_CoTkNoKx4wfpqfWXRCC6a4tCsqSYdV_v6G08Rpy3ZUqSqXW9hB7oSfA7lmE_Y7vYI9LpH6wKS6hhEMqnRPhvlyyOotsh-ANGEEkgVQvvo0dFUAgt81cmbnJSOrsRBuQn_HEZ1'
text="$@"
curl -H "Authorization: Bearer "$token \
        -H "Content-Type: application/json; charset=utf-8" \
        --data "{
            'input':{
              'ssml':'<speak>
                <break time=\'1.5s\' />
                  $text
                <break time=\'1s\' />
              </speak>'
            },
            'voice':{
              'languageCode':'en-US',
              'name':'en-US-Wavenet-I'
            },
            'audioConfig':{
              'audioEncoding':'LINEAR16',
              'speakingRate': 1,
              'effectsProfileId':['large-home-entertainment-class-device']
            }
        }" "https://texttospeech.googleapis.com/v1/text:synthesize" \
    | grep -o -E "\"audioContent\":\s?(\"\S+\")" \
    | awk -F':' '{gsub(/\"/,"",$2);print$2}' \
    | base64 -di > andro/andro.wav

if [ -s andro/andro.wav ]; then
    path=$(pwd)
    if [ -f "$(pwd)/andro/demo.sh" ]; then
        if [ -f "$(which conda)" ]; then
            conda=$(conda info --base)
            . $conda/etc/profile.d/conda.sh
            conda env create -f $path/andro/andro.yml &> /dev/null
            conda activate andro
        else
            cat << 'EOM'
Anaconda is not installed!
Install Anaconda first and then re-run this script.

Check official installation guide:

    https://docs.anaconda.com/anaconda/install/linux/

EOM
            exit 1
        fi
        cd $path/andro && bash ./demo.sh
    else
        printf "Please run this script from the folder you were in when installing Andro!\n"
    fi
else
    printf "\nWARNING: Audio file wasn't created due to network connection issues!\n"
    printf "Hint: Create speech WAV file and use it for video generation.\n\n"
fi